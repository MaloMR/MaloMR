# MASSIEU--ROCABOIS Malo

### 🇫🇷 French Backend Developer

I'm a 20 years old IT student at the [institute of technology of Vannes](https://www.iutvannes.fr/welcome-to-iut-vannes/), France.

I'm also a Java developer at [CA-TS](https://www.credit-agricole.com/marques-et-metiers/toutes-nos-marques/credit-agricole-technologies-et-services), Vannes, France.
